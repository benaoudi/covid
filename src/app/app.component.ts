import { Component, OnInit } from '@angular/core';
import { AnyMxRecord, AnyTxtRecord } from 'dns';
import { $ } from 'protractor';
import { SumaryService } from 'src/app/sumary.service'
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private sumary: SumaryService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  title = 'covid';
  countries:any;
  totalCases:any;
  newCases:any;
  activeCases:any
  totalRecovered:any;
  newRecovered:any;
  totalDeaths:any;
  newDeaths:any;
  recovery:any;
  mortality:any;

  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };

  public pieChartLabels: Label[] = [['Deaths'], ['Recovered'], ['Active']];
  public pieChartData: SingleDataSet = [0,0,0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [];

  ngOnInit() {
    this.sumary.getCountries().subscribe((data)=>{
    this.countries = data
    })
    this.sumary.getGlobalData().subscribe(data  => {
      this.totalCases = data.Global.TotalConfirmed
      this.newCases = data.Global.NewConfirmed
      this.activeCases = data.Global.TotalConfirmed - data.Global.TotalRecovered - data.Global.TotalDeaths
      this.totalRecovered = data.Global.TotalRecovered
      this.newRecovered = data.Global.NewRecovered
      this.totalDeaths = data.Global.TotalDeaths
      this.newDeaths = data.Global.NewDeaths
      this.recovery = Math.floor((((data.Global.TotalConfirmed - this.activeCases - data.Global.TotalDeaths) / data.Global.TotalConfirmed) * 100) * 100) / 100
      this.mortality = Math.floor((((this.totalCases - this.activeCases - this.totalRecovered) / this.totalCases) * 100) * 100) / 100
      
      this.pieChartData = [this.totalDeaths, this.totalRecovered, this.activeCases]

      this.get7DaysData();
      this.getCountryData();
    })
  }

  public countryData:Array<any>
  public All:Array<any> = [];

  async getCountryData() {
    const url = "https://api.covid19api.com/summary"

    let response = await fetch(url);
    let data = await response.json();

    let valueList = Object.values(data.Countries)
    let len = valueList.length;

    console.log(len)
    
    for (let i = 0; i < len; i++) {
      let tmp = Object.values(Object.values(data.Countries)[i]) // Store the data for the ith country from the API
      let tmpList:Array<any> = []
      tmpList.push(tmp[1])
      tmpList.push(tmp[4])
      tmpList.push(tmp[5])
      tmpList.push(tmp[6])
      tmpList.push(tmp[7])
      tmpList.push(tmp[8])
      tmpList.push(tmp[9])

      this.All.push(tmpList)
    }

    console.log(this.All)
  }

  async get7DaysData() {
    const url = "https://corona.lmao.ninja/v2/historical/all"
    let response = await fetch(url)
    let data = await response.json();

    let len = Object.values(data.cases).length;
    let dates = Object.keys(data.cases).slice(len - 7)

    let casesD = Object.values(data.cases)
    let recoveredD = Object.values(data.recovered)
    let deathsD = Object.values(data.deaths)

    let cases: Array<any> = casesD.slice(len - 7)
    let deaths: Array<any> = deathsD.slice(len - 7)
    let recovered: Array<any> = recoveredD.slice(len - 7)

    let newRecovered: Array<any> = []
    let newDeaths: Array<any> = []
    let newDaily = []

    for (let i = 0; i < 6; i++) {
      newDeaths.push(deaths[i+1] - deaths[i])
      newRecovered.push(recovered[i+1] - recovered[i])
      newDaily.push(cases[i+1] - cases[i])
    }

    this.barChartLabels = dates
    this.barChartData = [
      { data: newDeaths, label: 'Daily Deaths' },
      { data: newRecovered, label: 'Daily Recoveries' },
      { data: newDaily, label: 'Daily New Cases' }      
    ]

  }

  async getCountriesAllData() {
    const url = "https://disease.sh/v3/covid-19/historical?lastdays=all"
    let response = await fetch(url);
    let data = await response.json();

    
  }
}
