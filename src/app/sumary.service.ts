import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SumaryService {

  constructor(private http: HttpClient) { }

  getCountries() : Observable<any> {
    const url = "https://api.covid19api.com/countries"
    return this.http.get<any>(url);
  }

  getGlobalData(): Observable<any>{
    const url = "https://api.covid19api.com/summary"
    return this.http.get<any>(url);
  }

  get7Days() : Observable<any>{
    const url = "https://corona.lmao.ninja/v2/historical/all"
    return this.http.get<any>(url);
  }
}
