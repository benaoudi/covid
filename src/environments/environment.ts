// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAYAi-a_zSk2s4JthoTbWQ0dQTcs1VqtQk",
    authDomain: "my-expenses-b4659.firebaseapp.com",
    projectId: "my-expenses-b4659",
    storageBucket: "my-expenses-b4659.appspot.com",
    messagingSenderId: "31927592071",
    appId: "1:31927592071:web:442fffc707c152749e0422",
    measurementId: "G-PFEXTF5P46"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
